/*! \file gsm.c \brief GSM function library. */
//*****************************************************************************
//
// File Name	: 'gsm.c'
// Title		: GSM functions
// Author		: Fahad Mirza - Copyright (C) 2012-2014
// Created		: 2012-01-08
// Revised		: 2012-04-12
// Version		: 1.1
// Target MCU	: Atmel AVR series
//
// This code is distributed under the GNU Public License
// which can be found at http://www.gnu.org/licenses/gpl.txt
//
//*****************************************************************************
#define F_CPU 16000000UL

#include <avr/io.h>
#include <util/delay.h>
#include <avr/interrupt.h>
#include <string.h>
#include "lcd_lib.h"
#include "gsm.h"



/////////////////////////////////////////////////// Possible GSM Replies ////////////////////////////////////////////////

const unsigned char  CR_LF[]	= "\r\n";				  // Carriage Return Line Feed
const unsigned char	 OK[]		= "OK\r\n";				  // OK string
const unsigned char  CREG[]		= "+CREG: 0,1\r\n";		  // Home network registered	
const unsigned char	 CALL_RDY[]	= "Call Ready\r\n";		  // Modem is ready
const unsigned char  RING[]		= "RING\r\n";			  // Incoming call				
const unsigned char  CMTI[]     = "+CMTI:";               // New Message arrived
const unsigned char  READY[]	= "> ";					  // Phone ready to receive text message
const unsigned char	 PWR_DWN[]	= "NORMAL POWER DOWN\r\n";// Power down the modem


/////////////////////////////////////////////////// Possible strings ////////////////////////////////////////////////
const unsigned char  INVALID[]  = "Invalid";              // Invalid msg received
const unsigned char  LIGHT_ON[] = "Light on";             // "Light on" msg from user
const unsigned char  LIGHT_OFF[]= "Light off";            // "Light off" msg from user
const unsigned char  STATUS[]   = "Status";               // Status of light
const unsigned char  PhnNo[]	= "+8801911193901";		  // Any phn no.



// Initialize pointer
const unsigned char  *searchStrings[7]  = {CR_LF, OK, CREG, CALL_RDY, CMTI, READY, PWR_DWN};


/////////////////////////////////////////// AT-Command set used ////////////////////////////////////////////////

const unsigned char  AT[]		= "AT\r";
const unsigned char  ATH[]		= "ATH\r";							// Hangup call
const unsigned char  ATE0[]     = "ATE0\r";                         // Echo off
const unsigned char  AT_CREG[]	= "AT+CREG?\r";						// Check Network
const unsigned char  AT_CMGF[]  = "AT+CMGF=1\r";                    // Text Mode
const unsigned char  AT_CNMI[]  = "AT+CNMI=2,1,0,0,0\r";            // Identification of new SMS
const unsigned char  AT_CPMS[]  = "AT+CPMS=\"SM\",\"SM\",\"SM\"\r"; // Preferred storage
const unsigned char  AT_CMGS[]	= "AT+CMGS=\"";						// Send message
const unsigned char  AT_CMGD[]	= "AT+CMGD=1\r";					// Delete message at index 1
const unsigned char  AT_CMGR[]	= "AT+CMGR=1\r";					// Read from index 1



//////////////////////////////////////////////// Debug variables ////////////////////////////////////////////////







/*  USART initialization
 *
 *  This function set correct baudrate and functionality of
 *  the USART.
 */

void usart_init( unsigned int baudrate, short dspeed )
{
	UBRRH = (unsigned char) (baudrate>>8);//Setting baudrate
    UBRRL = (unsigned char) baudrate;     //Setting baudrate
	
    if (dspeed == NormalSpeed)
    {
		UCSRA &= ~( 1 << U2X );			  
    }
	else
	{
		UCSRA |= ( 1 << U2X );			    //Double the speed
	}
	
    UCSRB |= ( 1 << RXEN ) | ( 1 << TXEN ); //Enable receiver and transmitter
    UCSRC |= ( 1 << URSEL ) | ( 1 << UCSZ1 ) | ( 1 << UCSZ0 );  //8 data bit
	
	
	//////////// Reset Variable ///////////
	sec_count = 0;    // increased at timer0 interrupt
	//////////////////////////////////////
	
	usart_rx_reset();        // Reset receive buffer
	usart_rx_on();			 // Enable Rx interrupt
	sei();		  
}




/*  
 *  RX interrupt disable + reset all variable
 *  
 */

void usart_rx_reset( void )
{
    UCSRB &= ~(1<<RXCIE);         // Disable RX interrupt
    rx_i = rx_wr_i = 0;           // Reset variables
    rx_overflow = rx_ack = 0;     
    rx_buffer[ rx_wr_i ] = '\0';  
}


/*  
 *  RX interrupt disable
 *  
 *  This function disable usart's Rx interrupt  
 */
void usart_rx_off( void )
{
	UCSRB &= ~(1<<RXCIE);        // Disable RX interrupt
}


/*  
 *	RX interrupt enable
 *
 *  This function enable usart's Rx interrupt
 */

void usart_rx_on( void )
{
    UCSRB |= ( 1 << RXCIE );
}




/*  
 *	Initialize the modem
 *
 *  This function start and config the modem
 */
int modem_init()
{
    _delay_ms(100);
	LCDclr();
	_delay_ms(100);
	
	SetSearchString( CREG_ );
			
	LCDGotoString(0,0, "Check Network..");
	Delay_s(1);
		
	check_network:
	usart_putStr(AT_CREG);
	usart_rx_on();
		
	if (check_acknowledge(7) > 0)
	{
		SetSearchString( OK_ );                //Set OK to be search string
		usart_putStr( ATE0 );                  //Send turn echo off
		usart_rx_on();                         //Receiver on

		if( check_acknowledge(5) > 0 )         //Echo off = OK
		{
			usart_putStr(AT_CMGF);             //Send Text Mode
			usart_rx_on();                     //Receiver on

    		if( check_acknowledge(5) > 0 )           
   			{
    			usart_putStr(AT_CPMS);         //Send preferred storage
       			usart_rx_on();                      

        		if( check_acknowledge(8) > 0 ) //Preferred storage = OK
        		{
        			usart_putStr(AT_CNMI);     //Send preferred indication of new messages
        			usart_rx_on();                  

           			if( check_acknowledge(8) > 0 )   //Preferred indication = OK
					{
				    	return 1;
					}
					else                            
		        	{
				    	// CNMI failed
					}
       			}
       			else                                
       			{
           			// CPMS failed
       			}
   			}
   			else                                    
   			{
       			// CMGF failed
   			}
		} 
		else
		{
			// ATE0 failed
		}
	}
	else
	{
		goto check_network;
	}
}





/*  
 *	Set desired search string
 */

void SetSearchString( unsigned char Response )
{
    usart_rx_off();				          // Disable RX interrupt
	searchFor = searchStrings[Response];  //Set desired search string
    searchStr = Response;                 //Used in rx_ISR
    rx_i = 0;
}




/*  
 *  Print string through USART
 */

void usart_putStr( const unsigned char *str )
{
    for( ;*str != '\0'; )
    {
        usart_putchar( *str++ );
    }
}




/* 
 *	Put char in transmit buffer
 */

short usart_putchar( unsigned char data )
{

    unsigned int i;

    for(i=0; !(UCSRA & (1<<UDRE)); i++)// Wait for empty transmit buffer
    {
        if( i > TX_WAIT )              // How long one should wait
        {
            return -1;                 // Transmission fail, Give feedback to function caller
        }
    }
    UDR = data;                        // Start transmition 	
}




/*  Check acknowledge returned from modem
 *
 *  This function checks if an acknowledge
 *  has been received from the phone. A counting loop is also
 *  included to avoid waiting for a acknowledge that never arrives.
 *
 *	Return Value:
 *     1 Success, correct acknowledge
 *     0 Error, returned "ERROR" or timed out
 */

int check_acknowledge( unsigned short timeOut)
{
	sec_count = 0;
	
	//Wait loop
	for( ;( rx_ack == 0 ) && ( sec_count < timeOut ); )
	{
		
		/////// for debug ////////
		char str[3];
		itoa(sec_count,str,10);
		LCDGotoString(14,1,str);
		//////////////////////////
		
		_delay_ms(100);
	}						

	if( rx_ack > 0 )        //Everything worked out fine
    {
        rx_ack = 0;         //Reset flag and return 1
        return 1;
    }
    else                   //A timeout could result from no acknowledge, wrong acknowledge or buffer overrun
    {
        usart_rx_reset();  //Reset buffer and interrupt routine
        return 0;          //Timed out, or wrong acknowledge from phone
    }
	
} 





/*  Send message
 *
 *  This function will take user defined message and phn no.
 *  If successful, the message will be forwarded to the connected GSM modem.
 *
 *	Return Value:
 *     1 Success, message sent
 *    -1 No "> " from phone
 *    -2 No message sent acknowledge
 */

int send_msg( unsigned char* str, unsigned char* no )
{
	SetSearchString( READY_ );      //Set READY to be search string
	usart_putStr( AT_CMGS );        //Send AT command
	usart_putStr(no);				//Send Phn no
	usart_putStr("\"\r");			//Send CR
    usart_rx_on();                  //Receiver on
	
	if( check_acknowledge(6) > 0 )	// Get ">"
	{
		SetSearchString( OK_ );	
		usart_putStr(str);			//Send msg
		usart_putchar( 26 );
		usart_rx_on();

		if( check_acknowledge(30) > 0 ) //Send SMS successfully
		{
			return 1;
		}
		else
		{
			// Msg sending failed
		}
	}
	else
	{
		// No "> "
	}
	

}



/*  Delay function
 *
 *  One second delay function. 
 *  Parameter takes the value of second.
 */

void Delay_s(char sec)
{
	char i,j;
	for(j=0; j<sec; j++)
	{
		for(i=0; i<10; i++)
		{
			_delay_ms(100);
		}
	}
}


/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

/*  Read message from a given index
 *
 *  This function is used to read a newly arrived message
 *  from index 1. The message is decoded, and stored
 *  in the msgbuff.
 */

unsigned char* read_msg( void )
{
    unsigned char *msg_str;        //Pointer to message
    unsigned int i;

    msg_str = '\0';

    usart_rx_reset();              //Reset system
    SetSearchString( OK_ );        //Set OK to be search string
    usart_putStr( AT_CMGR );       //Read message
    usart_rx_on();                 //Receiver on, wait for acknowledge

    if( check_acknowledge(10) > 0 )//Read = OK
    {
        msg_str = get_msg();       //Get message from the data returned from modem
		
		return msg_str;
    }
    else                           //Read != OK
    {
        return '\0';
    }

}




/*  Get start of compressed string
 *
 *  When a new message has been read from a given index, this function
 *  will run through it, and find start of the user text.
 *
 */
unsigned char* get_msg( void )
{

    unsigned char *in_handle;
    int i, len;

    len       = str_trim();                                   //Trim off \r\nOK\r\n
    in_handle = str_gets();                                   //Get rx_buff

    in_handle += 5;                                           //Skip first \r\n

    for( i = 0; ( i < len ) && ( *in_handle++ != '\n' ); i++) //Loop until we get '\n'
    {
        ;
    }

    //Error
    if( i >= len )
    {
        in_handle = '\0';
    }

    return in_handle;
}



/*  Remove O, K, \r and \n
 *
 *  If the receive buffer have "OK" "\r" "\n"
 *  These characters will be deleted.
 *
 *	Return value:
 *   i   Length of trimmed buffer
 */

int str_trim( void )
{

    int i;
    unsigned char temp;

    for( i = rx_wr_i - 1; i >= 0; i--)     //Run through rx_buffer backwards
    {
        temp = rx_buffer[i];               //rx_buff[i] into temp
        if( ( temp != '\r' ) && ( temp != '\n' ) && ( temp != 'O' ) && ( temp != 'K' ) ) //If not equal to 'O', 'K', '\r' or '\n', break
        {
            break;                         //Do break
        }
    }

    rx_buffer[ i+1 ] = '\0';               //Terminate trimmed string

    return i;                              //Return new length
}


/*  
 *	Return pointer to receive buffer
 */

unsigned char* str_gets( void )
{
  return rx_buffer;
}



/*  Delete a message from a given index
 *
 *  This function will use the "AT+CMGD" command to delete
 *  the message @ index 1
 *
 *	Return Value:
 *     1 Success
 *     0 Error
 */
int delete_msg( void )
{

    usart_rx_reset();               //Reset system
    SetSearchString( OK_ );         //Set OK to be search string
    usart_putStr( AT_CMGD );        //Delete message
    usart_rx_on();                  //Receiver on

    if( check_acknowledge(5) > 0 )   //Delete = OK
    {
        return 1;
    }

    else                            //Delete != OK
    {
        return 0;
    }
}