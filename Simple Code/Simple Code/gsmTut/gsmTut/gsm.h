

/*! \file gsm.h \brief GSM function library. */
//*****************************************************************************
//
// File Name	: 'gsm.h'
// Title		: GSM functions
// Author		: Fahad Mirza - Copyright (C) 2012-2014
// Created		: 2012-01-08
// Revised		: 2012-04-12
// Version		: 1.1
// Target MCU	: Atmel AVR series
//
// This code is distributed under the GNU Public License
// which can be found at http://www.gnu.org/licenses/gpl.txt
//
/// \par Overview
///		This library provides an easy interface to the GSM module for SMS purpose only.
///		In the time of creating library, I have ATmega16 in my hand.
//
//****************************************************************************



#ifndef GSM_H
#define GSM_H

#define TX_WAIT        65000     // Timeout value
#define RX_BUFFER_SIZE 300		 // rx_buffer size
#define RX_BUFFER_MASK RX_BUFFER_SIZE-2
#define DoubleSpeed 1
#define NormalSpeed 0
		
// Used to look up in SetSearchString( Response )
#define CRLF_       0 
#define OK_         1    
#define CREG_		2    
#define CALLRDY_	3  
#define CMTI_       4                 
#define READY_      5                                            
#define PWRDWN_		6




// Overflow and acknowledge flag
volatile int rx_overflow, rx_ack;

// private buffer
volatile unsigned char rx_buffer[RX_BUFFER_SIZE];

// Private pointer
volatile unsigned char searchStr;
volatile unsigned char  *searchFor;           // Flash pointer

// Buffer counter
volatile int rx_i;

// Buffer write index
volatile int rx_wr_i;


// Debug variable, Count the second
volatile unsigned short sec_count;

//const unsigned char  CR_LF[]	= "\r\n";				  // Carriage Return Line Feed

void usart_init( unsigned int baudrate, short dspeed );
int modem_init( void );
void usart_rx_reset( void );
void usart_rx_on( void );
void usart_rx_off( void );
void SetSearchString( unsigned char Response );
void usart_putStr( const unsigned char *str  );
short usart_putchar( unsigned char data );
int check_acknowledge( unsigned short timeOut );
int send_msg( unsigned char* str, unsigned char* no );
unsigned char* read_msg( void );
unsigned char* get_msg( void );
int str_trim( void );
unsigned char* str_gets( void );
int delete_msg( void );
void Delay_s(char sec);

#endif