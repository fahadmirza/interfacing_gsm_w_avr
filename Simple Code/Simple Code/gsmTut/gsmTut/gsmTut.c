
/*
 * gsmTut.c
 *
 * Created: 29/6/2012 12:15:02 PM
 * Author: Fahad Mirza
 * Manager (techshopbd.com)
 * +8801833316060
 * fahadmirza80@yahoo.com
 * If you use this library just give us (techshopbd.com) the credit. That will suffice :) 
 *
 * This code is distributed under the GNU Public License
 * which can be found at http://www.gnu.org/licenses/gpl.txt
 */ 

#define F_CPU 16000000UL
#include <avr/io.h>
#include <util/delay.h>
#include <avr/interrupt.h>
#include "lcd_lib.h"
#include "gsm.h"

extern const unsigned char  *searchStrings[7];
extern const unsigned char  AT[];


/*  Receive interrupt routine
 *
 *  This ISR routine buffer incoming messages from the connected GSM modem.
 *  Also it check if the received string was an acknowledge.
 */

ISR (USART_RXC_vect)
{

    unsigned char data;                 //Local variable

	while ( !(UCSRA & (1<<RXC)) ) ;   
    data = UDR;                         //Always read something

    rx_buffer[ rx_wr_i++ ] = data;      //Store new data


    if( rx_wr_i > RX_BUFFER_MASK )      //Check for overflow
    {
        rx_wr_i = 0;                    //Reset write index
        //rx_overflow = 1;              //Set flag high
        //UCSRB &= ~( 1 << RXCIE );     //Disable RX interrupt
    }

    if( searchFor[rx_i] == data )       //Test response match
    {
        rx_i++;

        if( !searchFor[rx_i] )          //End of new_message string...received new message!
        {
            rx_i = 0;

            if( searchStr == CMTI_ )    //+CMTI:
            {
                searchFor = searchStrings[ CRLF_ ]; //Wait for CR LF
                searchStr = CRLF_;
            }
            else                          //Normal acknowledgement
            {
                rx_ack = 1;               //Set new message flag
                UCSRB &= ~( 1 << RXCIE ); // Disable RX interrupt
            }
        }
    }

    else
    {
        rx_i = 0;                        //Not valid search pattern...start again.
    }
}



ISR(TIMER1_COMPA_vect)
{
	sec_count++;
	
	TIFR |= (1<<OCF1A);
}




int main(void)
{
    LCDinit();
	LCDcursorOFF();
	LCDhome();
	_delay_ms(100);
	LCDclr();
	_delay_ms(100);
	LCDGotoString(0,0, "Initializing..");
	Delay_s(1);
	
	
	// Timer1 Initialization
	TCCR1B |= (1<<WGM12) | (1<<CS10) | (1<<CS12);  // CTC mode, Prescaler 1024
	OCR1AH = 0x3D;			// For 1s OCR value is 15625 (0x3D09)
	OCR1AL = 0x09;
	TCNT1H = 0;
	TCNT1L = 0;
	TIMSK |= (1<<OCIE1A);	// Enable CTC interrupt
	////////////////////////////////////////////////////
	
	
	usart_init( 207, DoubleSpeed );//Baud rate @ 9600bps, with double speed
    usart_rx_reset();			  //Reset receive buffer after modem_init()
	SetSearchString( OK_ );		  // We will search for 'OK'
	
	LCDclr();
	
	while(1)
    {
         usart_rx_on();                //Ready to receive
		 usart_putStr(AT);	
		 if(check_acknowledge(5))  //I think within 5 sec modem will reply.
		 {
			LCDGotoString(0,1, "AT ok");
			Delay_s(1);
			LCDclr();
			Delay_s(1);
		 }
		 
		 else
		 {
			LCDGotoString(0,1, "AT fail");
			Delay_s(1);
			LCDclr();
			Delay_s(1);
		 }		 
    }
}